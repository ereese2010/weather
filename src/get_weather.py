import pandas as pd
import requests, json, os, sys
from datetime import datetime
from statistics import mean

current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(parent)
import config.config as config1



# print(parent)
urls = config1.configuration(filename = parent + "\config\settings.ini", section = 'url')


def c_to_f(c):
    return c * (9/5) + 32

def current_conditions():
    response = requests.get(urls['current_conditions']).json()
    print(response['properties']['timestamp'])
    print(c_to_f(response['properties']['temperature']['value']))

def forecast_hourly():
    response = requests.get(urls['forecast_hourly']).json()
    properties = response['properties']
    temps_by_hour = {}
    time_list = []
    temp_list = []
    for period in properties['periods']:
        startTime = datetime.strptime(period['startTime'][:19], "%Y-%m-%dT%H:%M:%S")
        time_list.append(startTime)
        temp_list.append(period['temperature'])
        temps_by_hour[startTime] = period['temperature']
    return temps_by_hour




def average_temp_by_day(temps_by_hour):

    for day in set([a.day for a in temps_by_hour.keys()]):
        hours = ([b for a, b in temps_by_hour.items() if a.day == day])
        print(round(mean(hours), 2))
    




temps_by_hour = forecast_hourly()
average_temp_by_day(temps_by_hour)

max_temp = max(temps_by_hour.values())
max_time = {i for i in temps_by_hour if temps_by_hour[i]==max_temp}


min_temp = min(temps_by_hour.values())
min_time = {i for i in temps_by_hour if temps_by_hour[i]==min_temp}

# print("Max Temp:", max_temp, [[a.day, a.hour] for a in max_time])
# print("Min Temp:", min_temp, [[a.day, a.hour] for a in min_time])





