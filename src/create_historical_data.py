from lib2to3.pgen2.pgen import DFAState
import pandas as pd
import os
path = "data"
file_name = 'combined.csv'
if os.path.isfile(path + "\\" + file_name):
    os.remove(path + "\\" + file_name)

files = os.listdir(path)

df = pd.DataFrame()

for file in files:
    df_temp = pd.read_csv(path + "\\" + file, header=0, 
                 names=['date', 'max_temp', 'min_temp', 'precip', 'snow', 'snow_depth'],
                 parse_dates=['date'], infer_datetime_format=True)
    
    df_temp.replace('T', 0, inplace=True)
    df_temp.replace('M', None, inplace=True)


    df_temp.max_temp = pd.to_numeric(df_temp.max_temp)
    df_temp.min_temp = pd.to_numeric(df_temp.min_temp)
    df_temp.precip = pd.to_numeric(df_temp.precip)
    df_temp.snow = pd.to_numeric(df_temp.snow)
    df_temp.snow_depth = pd.to_numeric(df_temp.snow_depth)
    print(df_temp.dtypes)          
    df = pd.concat([df, df_temp], axis=0)


# df.columns = [['date', 'max_temp', 'min_temp', 'precip', 'snow', 'snow_depth']]



# df.date = pd.to_datetime(df.date, format="%Y-%m-%d")
# df['date'] = pd.to_datetime(df['date'])
# df['date'] = pd.to_datetime(df['date'], infer_datetime_format=True)

print('final', df.dtypes)
df.dropna().to_csv(path + "\\" + file_name, index=False)


