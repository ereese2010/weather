import pandas as pd
import os


df = pd.read_csv(r'data\combined.csv')

max_snow_depth = (df.snow_depth.max())
max_snow_depth_date = df.date[df.snow_depth == max_snow_depth]

max_temp = (df.max_temp.max())
max_temp_date = df.date[df.max_temp == max_temp]

min_temp = (df.min_temp.min())
min_temp_date = df.date[df.min_temp == min_temp]

max_snow = (df.snow.max())
max_snow_date = df.date[df.snow == max_snow]

max_precip = (df.precip.max())
max_precip_date = df.date[df.precip == max_precip]

def get_extremes():
    max_temps = df[['date', 'max_temp']].sort_values(by=['max_temp'], ascending=False, ignore_index = True)
    max_snows = df[['date', 'snow_depth']].sort_values(by=['snow_depth'], ascending=False, ignore_index = True)
    max_precips = df[['date', 'precip']].sort_values(by=['precip'], ascending=False, ignore_index = True)
    min_temps = df[['date', 'min_temp']].sort_values(by=['min_temp'], ascending=True, ignore_index = True)
    max_snow_depths = df[['date', 'snow_depth']].sort_values(by=['snow_depth'], ascending=False, ignore_index = True)
    df_display = pd.concat([max_temps, min_temps, max_snows, max_precips, max_snow_depths], axis=1)
    return df_display


clear = lambda: os.system('cls')
clear()

# print("Max Snow Depth:", max_snow_depth, [a for a in max_snow_depth_date])
# print("Max Snow:", max_snow, [a for a in max_snow_date])
# print("Max precip:", max_precip, [a for a in max_precip_date])
# print("Max Temp:", max_temp, [a for a in max_temp_date])
# print("Min Temp:", min_temp, [a for a in min_temp_date])



# print(df_display.head(50))

