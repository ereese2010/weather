from flask import Flask, render_template
import src.analyze_data as ad
import pandas as pd

app = Flask(__name__)

@app.route("/")
def extremes():
    return ad.get_extremes().head(10).to_html() 


if __name__ == '__main__':
    app.run(debug=True)